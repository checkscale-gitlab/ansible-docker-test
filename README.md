# Local Ansible testing in Docker

This is just a quick setup to allow testing of Ansible playbooks in a Docker container.  

Uses the Ansible [docker connection plugin](https://docs.ansible.com/ansible/latest/plugins/connection/docker.html), so there's no need to mess about with SSH.

Uses Debian9 as I'm aiming for at least some vague compatibility with default Google Cloud instances.

## How it works

The script `test-ansible.sh` is extremely simple:

1. Build a docker image (Debian9 currently)
2. Run a container
3. Execute `ansible/playbook.yml`
4. Stop and remove the container

## Usage

* Clone repo
* `cd ansible-docker-test`
* `./test-ansible.sh`

Contains a simple Apache2 playbook.

## Credits

Hat-tip to [this post](https://medium.com/@ayeshasilvia/testing-ansible-playbook-in-a-docker-container-21628e9ee256) (Medium warning!)
